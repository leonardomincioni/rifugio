<%@page import="it.pegaso.animaliGit.model.Specie"%>
<%@page import="java.util.List"%>
<%@page import="it.pegaso.animaliGit.dao.SpecieDao"%>
<html>
<body>

	<% SpecieDao dao = new SpecieDao();
		List<Specie> list = dao.getAll();%>
		
		<form action="GetRazze">
			
			<h1>Adotta un animale</h1>
			<select id="specie" name="specie">
			
				<% for(Specie s: list){%>
				
				<option value="<%=s.getId()%>"><%=s.getDescrizione() %></option>
				
				<% }%>
			
			</select>
			
			<input type="submit">
			
		</form>
	
</body>
</html>
