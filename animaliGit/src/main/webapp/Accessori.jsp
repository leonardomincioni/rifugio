<%@page import="it.pegaso.animaliGit.model.Accessorio"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Accessori</title>
</head>
<body>

	<% List<Accessorio> list = (List<Accessorio>)request.getAttribute("list"); %>
	
	<table>
		<thead>
			<th>Descrizione</th>
			<th>Prezzo</th>
		</thead>
		<tbody>
		<% for(Accessorio a : list){ %>
		
		<tr>
			<td><%= a.getDescrizione() %></td>
			<td><%= a.getPrezzo() %></td>
		</tr>
		
		<%} %>
		</tbody>
	</table>

</body>
</html>