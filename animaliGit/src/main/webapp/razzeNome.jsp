<%@page import="it.pegaso.animaliGit.model.Razza"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Nome e razza</title>
</head>
<body>
	<% List<Razza> lista = (List<Razza>)request.getAttribute("lista"); %>
	
	<form action="adoptAnimal" method="post">
		<input type="text" name="nome">
		<select name="razza">
		<% for (Razza r : lista){ %>
			<option value="<%=r.getId()%><%=r.getDescrizione() %>"></option>
		<%} %>
		</select>
		<input type="submit">
	</form>
</body>
</html>