package it.pegaso.animaliGit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.protocol.Resultset;

import it.pegaso.animaliGit.model.Specie;


public class SpecieDao {
	
	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root";
	private Connection connection;
	private PreparedStatement getById;
	
	public Specie getSpecieById(int id) {
		Specie result = null;
		
		try {
			getGetById().clearParameters();
			getGetById().setInt(1, id);
			
			ResultSet rs = getGetById().executeQuery();
			
			if (rs.next()) {
				result = new Specie();
				result.setId(id);
				result.setDescrizione(rs.getString("descrizione"));
			
				
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	public List<Specie> getAll(){
		List<Specie> result = new ArrayList<Specie>();
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("select * from specie");
			while(rs.next()) {
				Specie s = new Specie();
				s.setId(rs.getInt("id"));
				s.setDescrizione(rs.getString("descrizione"));
				result.add(s);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
			
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}


	public PreparedStatement getGetById() throws ClassNotFoundException, SQLException {
		if(getById == null) {
			getById = getConnection().prepareStatement("select * from specie where id = ? ");
		}
		return getById;
	}


	public void setGetById(PreparedStatement getByRazza) {
		this.getById = getByRazza;
	}
	
	

}
