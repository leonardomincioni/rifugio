package it.pegaso.animaliGit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.animaliGit.model.Accessorio;

public class AccessoriDao {

	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root";
	private Connection connection;
	private PreparedStatement bySpecie;
	
	public List<Accessorio> accessoriPerSpecie(int id){
		List<Accessorio> result =new ArrayList<Accessorio>();
		try {
			getBySpecie().clearParameters();
			getBySpecie().setInt(1, id);
			ResultSet rs = getBySpecie().executeQuery();
			
			while(rs.next()) {
				Accessorio a = new Accessorio();
				a.setId(id);
				a.setDescrizione(rs.getString("descrizione"));
				a.setIdSpecie(rs.getInt("idSpecie"));
				a.setPrezzo(rs.getInt("prezzo"));
				result.add(a);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	} 
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
			
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public PreparedStatement getBySpecie() throws ClassNotFoundException, SQLException {
		if(bySpecie == null) {
			bySpecie = getConnection().prepareStatement("select * from accessori where idSpecie= ?");
		}
		return bySpecie;
	}
	public void setBySpecie(PreparedStatement bySpecie) {
		this.bySpecie = bySpecie;
	}
	
	
	
}
