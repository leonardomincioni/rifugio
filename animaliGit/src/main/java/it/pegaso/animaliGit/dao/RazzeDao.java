package it.pegaso.animaliGit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import it.pegaso.animaliGit.model.Razza;

public class RazzeDao {

	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root";
	private Connection connection;
	private PreparedStatement razzaByIdSpecie;
	
	
	
	public List<Razza> getRazzeBySpecie(int idSpecie){
		List<Razza> result = new ArrayList<Razza>();
		
		try {
			getRazzaByIdSpecie().clearParameters();
			getRazzaByIdSpecie().setInt(1, idSpecie);
			ResultSet rs = getRazzaByIdSpecie().executeQuery();
			
			while(rs.next()) {
				Razza r = new Razza();
				r.setId(rs.getInt("id"));
				r.setDescrizione(rs.getString("descrizione"));
				r.setIdSpecie(rs.getInt(idSpecie));
				result.add(r);
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
			
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public PreparedStatement getRazzaByIdSpecie() throws ClassNotFoundException, SQLException {
		if(razzaByIdSpecie == null) {
			razzaByIdSpecie = getConnection().prepareStatement("select * from razza where idSpecie=?");
		}
		return razzaByIdSpecie;
	}
	public void setRazzaByIdSpecie(PreparedStatement razzaByIdSpecie) {
		this.razzaByIdSpecie = razzaByIdSpecie;
	}
	
	
}
