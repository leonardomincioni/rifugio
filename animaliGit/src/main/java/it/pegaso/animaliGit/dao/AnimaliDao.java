package it.pegaso.animaliGit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AnimaliDao {

	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/animali?user=root&password=root";
	private Connection connection;
	private PreparedStatement insert;
	
	public String insert(String nome, int idRazza) {
		try {
			getInsert().clearParameters();
			getInsert().setString(1, nome);
			getInsert().setInt(2, idRazza);
			getInsert().execute();
			return "Grazie per aver adottato " + nome;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "C'� stato qualche problema";
	}
	
	
	
	
	public PreparedStatement getInsert() throws ClassNotFoundException, SQLException {
		if(insert == null) {
			insert = getConnection().prepareStatement("insert into animale (nome, idRazza) values(?, ?)");
		}
		return insert;
	}
	public void setInsert(PreparedStatement insert) {
		this.insert = insert;
	}
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if(connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
			
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}
