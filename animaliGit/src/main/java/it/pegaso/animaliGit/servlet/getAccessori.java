package it.pegaso.animaliGit.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.animaliGit.dao.AccessoriDao;
import it.pegaso.animaliGit.dao.RazzeDao;
import it.pegaso.animaliGit.dao.SpecieDao;
import it.pegaso.animaliGit.model.Accessorio;
import it.pegaso.animaliGit.model.Razza;
import it.pegaso.animaliGit.model.Specie;

/**
 * Servlet implementation class getAccessori
 */
@WebServlet("/getAccessori")
public class getAccessori extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getAccessori() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AccessoriDao accessoriDao = new AccessoriDao();
		RazzeDao razzeDao = new RazzeDao();
		SpecieDao specieDao = new SpecieDao();
		
		String idRazza = request.getParameter("razza");
		Razza r = razzeDao.getRazzaById(Integer.parseInt(idRazza));
		Specie s = specieDao.getSpecieById(r.getIdSpecie());
		List<Accessorio> list = accessoriDao.accessoriPerSpecie(s.getId());
		
		request.setAttribute("list", list);
		RequestDispatcher rd = request.getRequestDispatcher("Accessori.jsp");
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
