package it.pegaso.animaliGit.model;

public class Animale {

	private int id;
	private String nome;
	private int idRazza;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdRazza() {
		return idRazza;
	}
	public void setIdRazza(int idRazza) {
		this.idRazza = idRazza;
	}
	
}
